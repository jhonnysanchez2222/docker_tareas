FROM eclipse-temurin:21
LABEL author=jhonny.com
COPY target/tareas-0.0.1-SNAPSHOT.jar tarea.jar
ENTRYPOINT ["java", "-jar", "/tarea.jar"]