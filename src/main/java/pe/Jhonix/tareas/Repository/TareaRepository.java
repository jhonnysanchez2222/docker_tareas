package pe.Jhonix.tareas.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import pe.Jhonix.tareas.model.Tarea;

public interface TareaRepository extends MongoRepository<Tarea, String> {
}
