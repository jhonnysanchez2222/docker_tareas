package pe.Jhonix.tareas.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pe.Jhonix.tareas.Repository.TareaRepository;
import pe.Jhonix.tareas.model.Tarea;

import java.util.List;

@CrossOrigin//para que habilite la lectura a Angular
@RestController
@RequestMapping("/tareas")
public class TareaController {

    @Autowired
    private TareaRepository tareaRepository;

    @GetMapping("/all")
    List<Tarea> index() {
        return tareaRepository.findAll();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/create")
    Tarea create(@RequestBody Tarea tarea) {
        System.out.println(tarea);
        return tareaRepository.save(tarea);
    }

    @PutMapping("/update/{id}")
    Tarea update(@PathVariable String id, @RequestBody Tarea tarea) {
        Tarea implTarea = tareaRepository.findById(id).orElse(null);
        implTarea.setName(tarea.getName());
        implTarea.setComplet(tarea.isComplet());
        return  tareaRepository.save(implTarea);

    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable String id){
        tareaRepository.deleteById(id);
        }

}
